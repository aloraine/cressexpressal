# This script will create a data file file which can be used to load data into mysql tables for coexpression 2.0
#This script takes a single argument at runtime. The argument specifies the path to the xml files from which the contents for the data file need to be picked up.
#eg. /Users/lorainelab/Nikhil_Dahake/GSE_XMLs/

import sys,urllib
import requests
from lxml import etree as ET
import re
import subprocess
import os
import glob
from xml.dom.minidom import parseString

XML_File_path=""
def main():
    createDataFile()
  

def createDataFile():
    
        DataFile="Data.txt" # This is the data file we are creating. Contents from xml will be read and written to this
        print "Please Wait..."
        arr= glob.glob(XML_File_path+"*.xml") #arr will store the names of all the xml files which need to be read 
        for xmlFile in arr:
            
           f = open(xmlFile, "r")
           doc = f.read()
           dom = parseString(doc)
            			
	   GSE_ID = dom.getElementsByTagName('Accession')[0].toxml();
           GSE_ID = GSE_ID.replace('<Accession>','').replace('</Accession>','') #GSE_ID stores the accession number
           Title = dom.getElementsByTagName('title')[0].toxml()
           Title = Title.replace('<title>','').replace('</title>','')
           Summary = dom.getElementsByTagName('summary')[0].toxml()
           Summary = Summary.replace('<summary>','').replace('</summary>','')
	   PDAT = dom.getElementsByTagName('PDAT')[0].toxml()
	   PDAT = PDAT.replace('<PDAT>','').replace('</PDAT>','')
	   n_samples = dom.getElementsByTagName('n_samples')[0].toxml()
	   n_samples = n_samples.replace('<n_samples>','').replace('</n_samples>','')
	   GDS = dom.getElementsByTagName('GDS')[0].toxml()
	   GDS = GDS.replace('<GDS>','').replace('</GDS>','')
	   GDS = GDS.replace('<GDS/>','');
    	   GDS = GDS.strip();

	   GSE_ID = GSE_ID.encode(sys.stdout.encoding, 'replace') #To handle some characters which cannot be represented correctly by unicode
           Title = Title.encode(sys.stdout.encoding, 'replace')
           Summary = Summary.encode(sys.stdout.encoding, 'replace')
	   PDAT = PDAT.encode(sys.stdout.encoding, 'replace')
	   n_samples = n_samples.encode(sys.stdout.encoding, 'replace')
	   GDS = GDS.encode(sys.stdout.encoding, 'replace')
	   GDS = GDS.strip();
            
            
                    
           try:
              
               
               f = open(XML_File_path+DataFile,'a')
               if GDS!= "":
                  if ";" in GDS:
                      GDS_List = GDS.split(";")

                      for GDS in GDS_List:
                          f.write(GSE_ID+"\t"+"GDS"+"\t"+GDS+"\t"+"GEO"+"\n")
                  else :
                        f.write(GSE_ID+"\t"+"GDS"+"\t"+GDS+"\t"+"GEO"+"\n")
               f.write(GSE_ID+"\t"+"Title"+"\t"+Title+"\t"+"GEO"+"\n")
	       f.write(GSE_ID+"\t"+"Summary"+"\t"+Summary+"\t"+"GEO"+"\n")
	       f.write(GSE_ID+"\t"+"PDAT"+"\t"+PDAT+"\t"+"GEO"+"\n")
	       f.write(GSE_ID+"\t"+"n_samples"+"\t"+n_samples+"\t"+"GEO"+"\n")
	       
               
               f.close
                             
                          
           except Exception as e:
                print e
                print "Something went wrong in the script!!"  
                
                                         
        print "The path for the data file is : "+XML_File_path+DataFile
        
         
    
if __name__ == '__main__':
      global XML_File_path
      XML_File_path=sys.argv[1]
            
      main()
