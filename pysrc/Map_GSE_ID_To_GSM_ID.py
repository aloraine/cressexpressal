#!/usr/bin/env python 

##################################
#This script maps all the GSM ids to the GSE they belong to. The script first fetches all the GSM ids from the server , finds their associated CEL files in the local directory 
#and if the file is found , writes the mapping of that GSM id to the GSE it belongs to.
#This file takes 3 parameters of which the last one is optional. They are as follows:

#1)Platform ID: The platform for which the GSM files need to be mapped to their GSE ids:
#2)Path To CEL files: Full path to existing CEL files for the given platform. eg "/Users/lorainelab/RawDataFiles/suppl/"
#3)Outputfile Name(Optional): Path to the output file to which the mapping will be written.

#The URL that we are fetching the CEL file info is :
#"http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=gds&term=GPL198[ACCN]+AND+gsm[ETYP]+AND+cel[suppFile]&retmax=100000&usehistory=y"
#We are fetching CEL files for the platform GPL198 in the above URL

import sys,urllib
import requests
from lxml import etree as ET
import re
import subprocess
import os
import glob

OutputFile="GSE_ID_To_GSM_ID_Mapping.txt"
PathToExistingCELfiles=""
def main(URL=None):
    getData(URL=URL)
  

def getData(URL=None):
        count =0
        headers={'accept':'application/xml'}
        response = requests.get(URL, headers=headers, stream=True)
        print 'Response Received'
        doc = ET.parse(response.raw)
        itemlist = doc.xpath("/eSearchResult/IdList/Id")# Extracting all the GSM IDs from the XML
        print "Total number of GSM IDs: "+str(len(itemlist))
        print "Please Wait..."
        f = open(OutputFile,'w')
        queryString = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=gds&id="
        id=""
        for s in itemlist:
            k = str(s.text)   
            for ch in range(1,len(k)): #This loop gets the GSM id after stripping off unneeded digits. eg 300967208 --> GSM967208
                if k[ch]!='0':
                    id=k[ch:]
                    break
            k = str(id)
            k = k[:len(k)-3]
            k=k+"nnn"
            
            #Check if GSM is present in our directory
                    
            try:
                
                arr= glob.glob(PathToExistingCELfiles+"[Gg][Ss][Mm]"+id+"*.[Cc][Ee][Ll].gz") #The GSM cel files have inconsistent names.
                GSM_ID="GSM"+id
                GSM_ID=str(GSM_ID)
                GSM_ID=GSM_ID.strip()
                CEL_file_exists=False
                for cel_file in arr:
                    #print ()
                    filename = os.path.basename(cel_file)
                    filename = filename.strip()
                    lengthOfGSM_ID=len(GSM_ID) #Here we check whether the CEL file we are associating with a GSM ID does indeed belong to that GSM ID. eg: GSM1234*.CEL.gz will return GSM1234.CEL.gz & GSM12345.CEL.gz and so on. So we take the first choice only
                    character = filename[lengthOfGSM_ID]
                    if character.isdigit(): #CEL file does not belong to the GSM ID in context
                        continue
                    else :
                        CEL_file_exists=True
                        break
                    
                if CEL_file_exists:
                    #fetch GSE for the GSM
                    try:
                        print "Retrieving info for :"+GSM_ID 
                        count=count+1
                        print count 
                        response = requests.get(queryString+s.text, headers=headers, stream=True) #s.text contains the id string. eg :300967208
                        doc = ET.parse(response.raw)
                        GSE_ID = doc.xpath("/eSummaryResult/DocSum/Item[@Name='GSE']")# Extracting the GSE ID from the XML
                        GSE_ID = "GSE"+str(GSE_ID[0].text)
                        GSE_ID=GSE_ID.strip()
                       # print(GSE_ID+"\t"+GSM_ID)
                        f.write(GSE_ID+"\t"+GSM_ID)
                    except Exception as e:
                        print e
                        f.write(str(e)+" for "+GSM_ID)    #logging the error into the file
                f.write("\n")
                          
            except Exception as e:
                print e
                print "Something went wrong in the script!!"  
                                         
        print "The mapping has been written to : "+OutputFile
       
         
    
if __name__ == '__main__':
      global OutputFile  
      global PathToExistingCELfiles
      platformID=sys.argv[1]
      PathToExistingCELfiles=sys.argv[2]
      if len(sys.argv)==4:
          OutputFile=str(sys.argv[3])
      url = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=gds&term="+platformID+"[ACCN]+AND+gsm[ETYP]+AND+cel[suppFile]&retmax=100000&usehistory=y"
      main(url)
