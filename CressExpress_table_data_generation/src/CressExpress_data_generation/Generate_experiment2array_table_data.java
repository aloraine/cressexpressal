package CressExpress_data_generation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author nikhildahake
 */
public class Generate_experiment2array_table_data {

    public static void main(String args[]) throws IOException {

        String readFile = "H:\\GSE_2_GSM_db_mapping.txt";
        String writeFile = "H:\\GSE_ID_to_GSM_ID_mapping.txt";

        FileReader reader = new FileReader(readFile);
        FileWriter writer = new FileWriter(writeFile);
        BufferedReader br = new BufferedReader(reader);
        BufferedWriter bw = new BufferedWriter(writer);
        int count = 0;
        String line = "";
        ArrayList<String> id = new ArrayList<String>();
        while ((line = br.readLine()) != null) {

            String arr[] = line.split("\t");

            String exp_id = arr[0].trim();
            String arr_id = arr[1].trim();
            String exp_name = arr[2].trim();
            String arr_name = arr[3].trim();

            bw.write(exp_id + "\t" + arr_id);
            bw.write("\n");
            count++;

        }
        bw.flush();

        br.close();
        bw.close();
        reader.close();
        writer.close();

        System.out.println(count);

    }
}
