package com.lorainelab.cressexpress.controller;

import com.lorainelab.cressexpress.controller.exceptions.NonexistentEntityException;
import com.lorainelab.cressexpress.facade.JobFacade;
import com.lorainelab.cressexpress.model.Job;
import com.lorainelab.cressexpress.model.ReleaseVersion;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Stateless
@LocalBean
public class JobController implements Serializable {

    @EJB
    private JobFacade ejbFacade;

    public JobController() {
    }

    private JobFacade getFacade() {
        return ejbFacade;
    }

    private EntityManager getEntityManager() {
        return getFacade().getEntityManager();
    }

    public void create(Job job) {
        EntityManager em = null;
        em = getEntityManager();
        ReleaseVersion releaseVersion = job.getReleaseVersion();
        if (releaseVersion != null) {
            releaseVersion = em.getReference(releaseVersion.getClass(), releaseVersion.getId());
            job.setReleaseVersion(releaseVersion);
        }
        em.persist(job);
        if (releaseVersion != null) {
            releaseVersion.getJobSet().add(job);
            releaseVersion = em.merge(releaseVersion);
        }

    }

    public void edit(Job job) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();

            Job persistentJob = em.find(Job.class, job.getId());
            ReleaseVersion releaseVersionOld = persistentJob.getReleaseVersion();
            ReleaseVersion releaseVersionNew = job.getReleaseVersion();
            if (releaseVersionNew != null) {
                releaseVersionNew = em.getReference(releaseVersionNew.getClass(), releaseVersionNew.getId());
                job.setReleaseVersion(releaseVersionNew);
            }
            job = em.merge(job);
            if (releaseVersionOld != null && !releaseVersionOld.equals(releaseVersionNew)) {
                releaseVersionOld.getJobSet().remove(job);
                releaseVersionOld = em.merge(releaseVersionOld);
            }
            if (releaseVersionNew != null && !releaseVersionNew.equals(releaseVersionOld)) {
                releaseVersionNew.getJobSet().add(job);
                releaseVersionNew = em.merge(releaseVersionNew);
            }

        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = job.getId();
                if (findJob(id) == null) {
                    throw new NonexistentEntityException("The job with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;

        em = getEntityManager();

        Job job;
        try {
            job = em.getReference(Job.class, id);
            job.getId();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The job with id " + id + " no longer exists.", enfe);
        }
        ReleaseVersion releaseVersion = job.getReleaseVersion();
        if (releaseVersion != null) {
            releaseVersion.getJobSet().remove(job);
            releaseVersion = em.merge(releaseVersion);
        }
        em.remove(job);

    }

    public List<Job> findJobEntities() {
        return findJobEntities(true, -1, -1);
    }

    public List<Job> findJobEntities(int maxResults, int firstResult) {
        return findJobEntities(false, maxResults, firstResult);
    }

    private List<Job> findJobEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();

        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Job.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();

    }

    public Job findJob(Integer id) {
        EntityManager em = getEntityManager();

        return em.find(Job.class, id);

    }

    public int getJobCount() {
        EntityManager em = getEntityManager();

        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Job> rt = cq.from(Job.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();

    }

}
