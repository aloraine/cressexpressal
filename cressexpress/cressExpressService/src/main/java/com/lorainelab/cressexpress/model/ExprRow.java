/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lorainelab.cressexpress.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dcnorris
 */
@Entity
@Table(name = "exprRow")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ExprRow.findAll", query = "SELECT e FROM ExprRow e"),
    @NamedQuery(name = "ExprRow.findById", query = "SELECT e FROM ExprRow e WHERE e.id = :id")})
public class ExprRow implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Lob
    @Column(name = "data")
    private String data;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "releaseVersion", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private ReleaseVersion releaseVersion;
    @JoinColumn(name = "probeset", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Probeset probeset;

    public ExprRow() {
    }

    public ExprRow(Integer id) {
        this.id = id;
    }

    public ExprRow(Integer id, String data) {
        this.id = id;
        this.data = data;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ReleaseVersion getReleaseVersion() {
        return releaseVersion;
    }

    public void setReleaseVersion(ReleaseVersion releaseVersion) {
        this.releaseVersion = releaseVersion;
    }

    public Probeset getProbeset() {
        return probeset;
    }

    public void setProbeset(Probeset probeset) {
        this.probeset = probeset;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExprRow)) {
            return false;
        }
        ExprRow other = (ExprRow) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lorainelab.cressdbbuilder.ExprRow[ id=" + id + " ]";
    }
    
}
