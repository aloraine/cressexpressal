/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lorainelab.cressexpress.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author dcnorris
 */
@Entity
@Table(name = "matchMethod")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MatchMethod.findAll", query = "SELECT m FROM MatchMethod m"),
    @NamedQuery(name = "MatchMethod.findById", query = "SELECT m FROM MatchMethod m WHERE m.id = :id"),
    @NamedQuery(name = "MatchMethod.findByName", query = "SELECT m FROM MatchMethod m WHERE m.name = :name"),
    @NamedQuery(name = "MatchMethod.findByNotes", query = "SELECT m FROM MatchMethod m WHERE m.notes = :notes")})
public class MatchMethod implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Column(name = "notes")
    private String notes;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "matchMethod")
    private Set<Probeset2geneMap> probeset2geneMapSet;

    public MatchMethod() {
    }

    public MatchMethod(Integer id) {
        this.id = id;
    }

    public MatchMethod(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @XmlTransient
    public Set<Probeset2geneMap> getProbeset2geneMapSet() {
        return probeset2geneMapSet;
    }

    public void setProbeset2geneMapSet(Set<Probeset2geneMap> probeset2geneMapSet) {
        this.probeset2geneMapSet = probeset2geneMapSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MatchMethod)) {
            return false;
        }
        MatchMethod other = (MatchMethod) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lorainelab.cressdbbuilder.MatchMethod[ id=" + id + " ]";
    }
    
}
