/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lorainelab.cressexpress.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author dcnorris
 */
@Entity
@Table(name = "releaseVersion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReleaseVersion.findAll", query = "SELECT r FROM ReleaseVersion r"),
    @NamedQuery(name = "ReleaseVersion.findById", query = "SELECT r FROM ReleaseVersion r WHERE r.id = :id"),
    @NamedQuery(name = "ReleaseVersion.findByName", query = "SELECT r FROM ReleaseVersion r WHERE r.name = :name"),
    @NamedQuery(name = "ReleaseVersion.findByDescr", query = "SELECT r FROM ReleaseVersion r WHERE r.descr = :descr")})
public class ReleaseVersion implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Column(name = "descr")
    private String descr;
    @Basic(optional = false)
    @Lob
    @Column(name = "arrayOrder")
    private String arrayOrder;
    @Basic(optional = false)
    @Lob
    @Column(name = "probesetOrder")
    private String probesetOrder;
    @JoinColumn(name = "platform", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Platform platform;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "releaseVersion")
    private Set<ExprColumn> exprColumnSet;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "releaseVersion")
    private Set<Job> jobSet;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "releaseVersion")
    private Set<ExprRow> exprRowSet;

    public ReleaseVersion() {
    }

    public ReleaseVersion(Integer id) {
        this.id = id;
    }

    public ReleaseVersion(Integer id, String name, String arrayOrder, String probesetOrder) {
        this.id = id;
        this.name = name;
        this.arrayOrder = arrayOrder;
        this.probesetOrder = probesetOrder;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getArrayOrder() {
        return arrayOrder;
    }

    public void setArrayOrder(String arrayOrder) {
        this.arrayOrder = arrayOrder;
    }

    public String getProbesetOrder() {
        return probesetOrder;
    }

    public void setProbesetOrder(String probesetOrder) {
        this.probesetOrder = probesetOrder;
    }

    public Platform getPlatform() {
        return platform;
    }

    public void setPlatform(Platform platform) {
        this.platform = platform;
    }

    @XmlTransient
    public Set<ExprColumn> getExprColumnSet() {
        return exprColumnSet;
    }

    public void setExprColumnSet(Set<ExprColumn> exprColumnSet) {
        this.exprColumnSet = exprColumnSet;
    }

    @XmlTransient
    public Set<Job> getJobSet() {
        return jobSet;
    }

    public void setJobSet(Set<Job> jobSet) {
        this.jobSet = jobSet;
    }

    @XmlTransient
    public Set<ExprRow> getExprRowSet() {
        return exprRowSet;
    }

    public void setExprRowSet(Set<ExprRow> exprRowSet) {
        this.exprRowSet = exprRowSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReleaseVersion)) {
            return false;
        }
        ReleaseVersion other = (ReleaseVersion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lorainelab.cressdbbuilder.ReleaseVersion[ id=" + id + " ]";
    }
    
}
