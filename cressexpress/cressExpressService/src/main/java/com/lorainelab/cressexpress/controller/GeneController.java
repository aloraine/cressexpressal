/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lorainelab.cressexpress.controller;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.lorainelab.cressexpress.controller.exceptions.NonexistentEntityException;
import com.lorainelab.cressexpress.controller.exceptions.PreexistingEntityException;
import com.lorainelab.cressexpress.model.Gene;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.lorainelab.cressexpress.model.Species;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;

/**
 *
 * @author dcnorris
 */
@Stateless
@LocalBean
public class GeneController implements Serializable {

    @EJB
    private com.lorainelab.cressexpress.facade.GeneFacade ejbFacade;

    public GeneController() {

    }
    
     public List<String> getValidGeneNames() {
       List<String> validNames = new ArrayList<>();
        for (Gene gene : findGeneEntities()) {
            validNames.add(gene.getName());
        }
        return validNames;
    }

    public BiMap<String, Integer> getGeneIdMap() {
        BiMap<String, Integer> geneIdMap = HashBiMap.create();
        for (Gene gene : findGeneEntities()) {
            geneIdMap.put(gene.getName(), gene.getId());
        }
        return geneIdMap;
    }

    public EntityManager getEntityManager() {
        return ejbFacade.getEntityManager();
    }

    public void create(Gene gene) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Species species = gene.getSpecies();
            if (species != null) {
                species = em.getReference(species.getClass(), species.getId());
                gene.setSpecies(species);
            }
            em.persist(gene);
            if (species != null) {
                species.getGenes().add(gene);
                species = em.merge(species);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findGene(gene.getId()) != null) {
                throw new PreexistingEntityException("Gene " + gene + " already exists.", ex);
            }
            throw ex;
        } 
    }

    public void edit(Gene gene) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Gene persistentGene = em.find(Gene.class, gene.getId());
            Species speciesOld = persistentGene.getSpecies();
            Species speciesNew = gene.getSpecies();
            if (speciesNew != null) {
                speciesNew = em.getReference(speciesNew.getClass(), speciesNew.getId());
                gene.setSpecies(speciesNew);
            }
            gene = em.merge(gene);
            if (speciesOld != null && !speciesOld.equals(speciesNew)) {
                speciesOld.getGenes().remove(gene);
                speciesOld = em.merge(speciesOld);
            }
            if (speciesNew != null && !speciesNew.equals(speciesOld)) {
                speciesNew.getGenes().add(gene);
                speciesNew = em.merge(speciesNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = gene.getId();
                if (findGene(id) == null) {
                    throw new NonexistentEntityException("The gene with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } 
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
     
            em = getEntityManager();
            em.getTransaction().begin();
            Gene gene;
            try {
                gene = em.getReference(Gene.class, id);
                gene.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The gene with id " + id + " no longer exists.", enfe);
            }
            Species species = gene.getSpecies();
            if (species != null) {
                species.getGenes().remove(gene);
                species = em.merge(species);
            }
            em.remove(gene);
            em.getTransaction().commit();
      
    }

    public List<Gene> findGeneEntities() {
        return findGeneEntities(true, -1, -1);
    }

    public List<Gene> findGeneEntities(int maxResults, int firstResult) {
        return findGeneEntities(false, maxResults, firstResult);
    }

    private List<Gene> findGeneEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Gene.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        
    }

    public Gene findGene(Integer id) {
        EntityManager em = getEntityManager();
       
            return em.find(Gene.class, id);
       
    }

    public int getGeneCount() {
        EntityManager em = getEntityManager();
       
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Gene> rt = cq.from(Gene.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        
    }

}
