package com.lorainelab.cressexpress.controller;

import com.lorainelab.cressexpress.controller.exceptions.IllegalOrphanException;
import com.lorainelab.cressexpress.controller.exceptions.NonexistentEntityException;
import com.lorainelab.cressexpress.controller.exceptions.PreexistingEntityException;
import com.lorainelab.cressexpress.facade.ReleaseVersionFacade;
import com.lorainelab.cressexpress.model.ExprColumn;
import com.lorainelab.cressexpress.model.ExprRow;
import com.lorainelab.cressexpress.model.Job;
import com.lorainelab.cressexpress.model.Platform;
import com.lorainelab.cressexpress.model.ReleaseVersion;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Stateless
@LocalBean
public class ReleaseVersionController implements Serializable {

    @EJB
    private ReleaseVersionFacade ejbFacade;

    public ReleaseVersionController() {
    }

    //Select descr from coexpression2.release where platform_id='1'
    public List<ReleaseVersion> getVersionByPlatformId(Integer platformId) {
        EntityManager entityManager = ejbFacade.getEntityManager();
        Query query = entityManager.createQuery("Select v from ReleaseVersion v WHERE v.platform.id= :platformId");
        query.setParameter("platformId", platformId);
        List<ReleaseVersion> results = query.getResultList();
        return results;
    }

    public ReleaseVersion getDefaultVersion() {
        EntityManager entityManager = ejbFacade.getEntityManager();
        Query query = entityManager.createQuery("Select v from ReleaseVersion v");
        query.setMaxResults(1);
        ReleaseVersion result = (ReleaseVersion) query.getSingleResult();
        return result;
    }

    public ReleaseVersionFacade getFacade() {
        return ejbFacade;
    }

    private EntityManager getEntityManager() {
        return getFacade().getEntityManager();
    }

    public void create(ReleaseVersion releaseVersion) throws PreexistingEntityException, Exception {
        if (releaseVersion.getExprColumnSet() == null) {
            releaseVersion.setExprColumnSet(new HashSet<ExprColumn>());
        }
        if (releaseVersion.getJobSet() == null) {
            releaseVersion.setJobSet(new HashSet<Job>());
        }
        if (releaseVersion.getExprRowSet() == null) {
            releaseVersion.setExprRowSet(new HashSet<ExprRow>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Platform platform = releaseVersion.getPlatform();
            if (platform != null) {
                platform = em.getReference(platform.getClass(), platform.getId());
                releaseVersion.setPlatform(platform);
            }
            Set<ExprColumn> attachedExprColumnSet = new HashSet<ExprColumn>();
            for (ExprColumn exprColumnSetExprColumnToAttach : releaseVersion.getExprColumnSet()) {
                exprColumnSetExprColumnToAttach = em.getReference(exprColumnSetExprColumnToAttach.getClass(), exprColumnSetExprColumnToAttach.getId());
                attachedExprColumnSet.add(exprColumnSetExprColumnToAttach);
            }
            releaseVersion.setExprColumnSet(attachedExprColumnSet);
            Set<Job> attachedJobSet = new HashSet<Job>();
            for (Job jobSetJobToAttach : releaseVersion.getJobSet()) {
                jobSetJobToAttach = em.getReference(jobSetJobToAttach.getClass(), jobSetJobToAttach.getId());
                attachedJobSet.add(jobSetJobToAttach);
            }
            releaseVersion.setJobSet(attachedJobSet);
            Set<ExprRow> attachedExprRowSet = new HashSet<ExprRow>();
            for (ExprRow exprRowSetExprRowToAttach : releaseVersion.getExprRowSet()) {
                exprRowSetExprRowToAttach = em.getReference(exprRowSetExprRowToAttach.getClass(), exprRowSetExprRowToAttach.getId());
                attachedExprRowSet.add(exprRowSetExprRowToAttach);
            }
            releaseVersion.setExprRowSet(attachedExprRowSet);
            em.persist(releaseVersion);
            if (platform != null) {
                platform.getReleaseVersionSet().add(releaseVersion);
                platform = em.merge(platform);
            }
            for (ExprColumn exprColumnSetExprColumn : releaseVersion.getExprColumnSet()) {
                ReleaseVersion oldReleaseVersionOfExprColumnSetExprColumn = exprColumnSetExprColumn.getReleaseVersion();
                exprColumnSetExprColumn.setReleaseVersion(releaseVersion);
                exprColumnSetExprColumn = em.merge(exprColumnSetExprColumn);
                if (oldReleaseVersionOfExprColumnSetExprColumn != null) {
                    oldReleaseVersionOfExprColumnSetExprColumn.getExprColumnSet().remove(exprColumnSetExprColumn);
                    oldReleaseVersionOfExprColumnSetExprColumn = em.merge(oldReleaseVersionOfExprColumnSetExprColumn);
                }
            }
            for (Job jobSetJob : releaseVersion.getJobSet()) {
                ReleaseVersion oldReleaseVersionOfJobSetJob = jobSetJob.getReleaseVersion();
                jobSetJob.setReleaseVersion(releaseVersion);
                jobSetJob = em.merge(jobSetJob);
                if (oldReleaseVersionOfJobSetJob != null) {
                    oldReleaseVersionOfJobSetJob.getJobSet().remove(jobSetJob);
                    oldReleaseVersionOfJobSetJob = em.merge(oldReleaseVersionOfJobSetJob);
                }
            }
            for (ExprRow exprRowSetExprRow : releaseVersion.getExprRowSet()) {
                ReleaseVersion oldReleaseVersionOfExprRowSetExprRow = exprRowSetExprRow.getReleaseVersion();
                exprRowSetExprRow.setReleaseVersion(releaseVersion);
                exprRowSetExprRow = em.merge(exprRowSetExprRow);
                if (oldReleaseVersionOfExprRowSetExprRow != null) {
                    oldReleaseVersionOfExprRowSetExprRow.getExprRowSet().remove(exprRowSetExprRow);
                    oldReleaseVersionOfExprRowSetExprRow = em.merge(oldReleaseVersionOfExprRowSetExprRow);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findReleaseVersion(releaseVersion.getId()) != null) {
                throw new PreexistingEntityException("ReleaseVersion " + releaseVersion + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(ReleaseVersion releaseVersion) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ReleaseVersion persistentReleaseVersion = em.find(ReleaseVersion.class, releaseVersion.getId());
            Platform platformOld = persistentReleaseVersion.getPlatform();
            Platform platformNew = releaseVersion.getPlatform();
            Set<ExprColumn> exprColumnSetOld = persistentReleaseVersion.getExprColumnSet();
            Set<ExprColumn> exprColumnSetNew = releaseVersion.getExprColumnSet();
            Set<Job> jobSetOld = persistentReleaseVersion.getJobSet();
            Set<Job> jobSetNew = releaseVersion.getJobSet();
            Set<ExprRow> exprRowSetOld = persistentReleaseVersion.getExprRowSet();
            Set<ExprRow> exprRowSetNew = releaseVersion.getExprRowSet();
            List<String> illegalOrphanMessages = null;
            for (ExprColumn exprColumnSetOldExprColumn : exprColumnSetOld) {
                if (!exprColumnSetNew.contains(exprColumnSetOldExprColumn)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain ExprColumn " + exprColumnSetOldExprColumn + " since its releaseVersion field is not nullable.");
                }
            }
            for (Job jobSetOldJob : jobSetOld) {
                if (!jobSetNew.contains(jobSetOldJob)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Job " + jobSetOldJob + " since its releaseVersion field is not nullable.");
                }
            }
            for (ExprRow exprRowSetOldExprRow : exprRowSetOld) {
                if (!exprRowSetNew.contains(exprRowSetOldExprRow)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain ExprRow " + exprRowSetOldExprRow + " since its releaseVersion field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (platformNew != null) {
                platformNew = em.getReference(platformNew.getClass(), platformNew.getId());
                releaseVersion.setPlatform(platformNew);
            }
            Set<ExprColumn> attachedExprColumnSetNew = new HashSet<ExprColumn>();
            for (ExprColumn exprColumnSetNewExprColumnToAttach : exprColumnSetNew) {
                exprColumnSetNewExprColumnToAttach = em.getReference(exprColumnSetNewExprColumnToAttach.getClass(), exprColumnSetNewExprColumnToAttach.getId());
                attachedExprColumnSetNew.add(exprColumnSetNewExprColumnToAttach);
            }
            exprColumnSetNew = attachedExprColumnSetNew;
            releaseVersion.setExprColumnSet(exprColumnSetNew);
            Set<Job> attachedJobSetNew = new HashSet<Job>();
            for (Job jobSetNewJobToAttach : jobSetNew) {
                jobSetNewJobToAttach = em.getReference(jobSetNewJobToAttach.getClass(), jobSetNewJobToAttach.getId());
                attachedJobSetNew.add(jobSetNewJobToAttach);
            }
            jobSetNew = attachedJobSetNew;
            releaseVersion.setJobSet(jobSetNew);
            Set<ExprRow> attachedExprRowSetNew = new HashSet<ExprRow>();
            for (ExprRow exprRowSetNewExprRowToAttach : exprRowSetNew) {
                exprRowSetNewExprRowToAttach = em.getReference(exprRowSetNewExprRowToAttach.getClass(), exprRowSetNewExprRowToAttach.getId());
                attachedExprRowSetNew.add(exprRowSetNewExprRowToAttach);
            }
            exprRowSetNew = attachedExprRowSetNew;
            releaseVersion.setExprRowSet(exprRowSetNew);
            releaseVersion = em.merge(releaseVersion);
            if (platformOld != null && !platformOld.equals(platformNew)) {
                platformOld.getReleaseVersionSet().remove(releaseVersion);
                platformOld = em.merge(platformOld);
            }
            if (platformNew != null && !platformNew.equals(platformOld)) {
                platformNew.getReleaseVersionSet().add(releaseVersion);
                platformNew = em.merge(platformNew);
            }
            for (ExprColumn exprColumnSetNewExprColumn : exprColumnSetNew) {
                if (!exprColumnSetOld.contains(exprColumnSetNewExprColumn)) {
                    ReleaseVersion oldReleaseVersionOfExprColumnSetNewExprColumn = exprColumnSetNewExprColumn.getReleaseVersion();
                    exprColumnSetNewExprColumn.setReleaseVersion(releaseVersion);
                    exprColumnSetNewExprColumn = em.merge(exprColumnSetNewExprColumn);
                    if (oldReleaseVersionOfExprColumnSetNewExprColumn != null && !oldReleaseVersionOfExprColumnSetNewExprColumn.equals(releaseVersion)) {
                        oldReleaseVersionOfExprColumnSetNewExprColumn.getExprColumnSet().remove(exprColumnSetNewExprColumn);
                        oldReleaseVersionOfExprColumnSetNewExprColumn = em.merge(oldReleaseVersionOfExprColumnSetNewExprColumn);
                    }
                }
            }
            for (Job jobSetNewJob : jobSetNew) {
                if (!jobSetOld.contains(jobSetNewJob)) {
                    ReleaseVersion oldReleaseVersionOfJobSetNewJob = jobSetNewJob.getReleaseVersion();
                    jobSetNewJob.setReleaseVersion(releaseVersion);
                    jobSetNewJob = em.merge(jobSetNewJob);
                    if (oldReleaseVersionOfJobSetNewJob != null && !oldReleaseVersionOfJobSetNewJob.equals(releaseVersion)) {
                        oldReleaseVersionOfJobSetNewJob.getJobSet().remove(jobSetNewJob);
                        oldReleaseVersionOfJobSetNewJob = em.merge(oldReleaseVersionOfJobSetNewJob);
                    }
                }
            }
            for (ExprRow exprRowSetNewExprRow : exprRowSetNew) {
                if (!exprRowSetOld.contains(exprRowSetNewExprRow)) {
                    ReleaseVersion oldReleaseVersionOfExprRowSetNewExprRow = exprRowSetNewExprRow.getReleaseVersion();
                    exprRowSetNewExprRow.setReleaseVersion(releaseVersion);
                    exprRowSetNewExprRow = em.merge(exprRowSetNewExprRow);
                    if (oldReleaseVersionOfExprRowSetNewExprRow != null && !oldReleaseVersionOfExprRowSetNewExprRow.equals(releaseVersion)) {
                        oldReleaseVersionOfExprRowSetNewExprRow.getExprRowSet().remove(exprRowSetNewExprRow);
                        oldReleaseVersionOfExprRowSetNewExprRow = em.merge(oldReleaseVersionOfExprRowSetNewExprRow);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = releaseVersion.getId();
                if (findReleaseVersion(id) == null) {
                    throw new NonexistentEntityException("The releaseVersion with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;

        em = getEntityManager();
        em.getTransaction().begin();
        ReleaseVersion releaseVersion;
        try {
            releaseVersion = em.getReference(ReleaseVersion.class, id);
            releaseVersion.getId();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The releaseVersion with id " + id + " no longer exists.", enfe);
        }
        List<String> illegalOrphanMessages = null;
        Set<ExprColumn> exprColumnSetOrphanCheck = releaseVersion.getExprColumnSet();
        for (ExprColumn exprColumnSetOrphanCheckExprColumn : exprColumnSetOrphanCheck) {
            if (illegalOrphanMessages == null) {
                illegalOrphanMessages = new ArrayList<String>();
            }
            illegalOrphanMessages.add("This ReleaseVersion (" + releaseVersion + ") cannot be destroyed since the ExprColumn " + exprColumnSetOrphanCheckExprColumn + " in its exprColumnSet field has a non-nullable releaseVersion field.");
        }
        Set<Job> jobSetOrphanCheck = releaseVersion.getJobSet();
        for (Job jobSetOrphanCheckJob : jobSetOrphanCheck) {
            if (illegalOrphanMessages == null) {
                illegalOrphanMessages = new ArrayList<String>();
            }
            illegalOrphanMessages.add("This ReleaseVersion (" + releaseVersion + ") cannot be destroyed since the Job " + jobSetOrphanCheckJob + " in its jobSet field has a non-nullable releaseVersion field.");
        }
        Set<ExprRow> exprRowSetOrphanCheck = releaseVersion.getExprRowSet();
        for (ExprRow exprRowSetOrphanCheckExprRow : exprRowSetOrphanCheck) {
            if (illegalOrphanMessages == null) {
                illegalOrphanMessages = new ArrayList<String>();
            }
            illegalOrphanMessages.add("This ReleaseVersion (" + releaseVersion + ") cannot be destroyed since the ExprRow " + exprRowSetOrphanCheckExprRow + " in its exprRowSet field has a non-nullable releaseVersion field.");
        }
        if (illegalOrphanMessages != null) {
            throw new IllegalOrphanException(illegalOrphanMessages);
        }
        Platform platform = releaseVersion.getPlatform();
        if (platform != null) {
            platform.getReleaseVersionSet().remove(releaseVersion);
            platform = em.merge(platform);
        }
        em.remove(releaseVersion);
        em.getTransaction().commit();

    }

    public List<ReleaseVersion> findReleaseVersionEntities() {
        return findReleaseVersionEntities(true, -1, -1);
    }

    public List<ReleaseVersion> findReleaseVersionEntities(int maxResults, int firstResult) {
        return findReleaseVersionEntities(false, maxResults, firstResult);
    }

    private List<ReleaseVersion> findReleaseVersionEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();

        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(ReleaseVersion.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();

    }

    public ReleaseVersion findReleaseVersion(Integer id) {
        EntityManager em = getEntityManager();

        return em.find(ReleaseVersion.class, id);

    }

    public int getReleaseVersionCount() {
        EntityManager em = getEntityManager();

        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<ReleaseVersion> rt = cq.from(ReleaseVersion.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();

    }

}
