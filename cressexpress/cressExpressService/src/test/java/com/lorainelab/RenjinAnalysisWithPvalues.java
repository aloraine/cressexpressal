/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lorainelab;

//import com.google.common.base.Charsets;
//import com.google.common.base.Splitter;
//import com.google.common.io.CharStreams;
//import com.google.common.primitives.Doubles;
//import java.io.File;
//import java.io.FileReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.io.Reader;
//import java.util.LinkedHashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.function.BiFunction;
//import java.util.function.Function;
//import static java.util.stream.Collectors.toList;
//import javax.script.ScriptEngine;
//import javax.script.ScriptEngineManager;
//import javax.script.ScriptException;
//import org.apache.commons.csv.CSVFormat;
//import org.apache.commons.csv.CSVRecord;
//import org.junit.Before;
//import org.junit.Test;
//import org.mapdb.DB;
//import org.mapdb.DBMaker;
//import org.renjin.sexp.SEXP;

/**
 *
 * @author dcnorris
 */
public class RenjinAnalysisWithPvalues {

//    List<String> microarrayOrder;
//    List<String> probesetNames;
//    List<String> microarraySelections;
//    List<String> probesetSelections;
//    double plcRsquaredThreshold = 0.36;
//    String expressionFlatFile = "/home/dcnorris/NetBeansProjects/rcressexpress/AllAgainstAll/cressExpressFlatFileDump/exprRowData.txt";

//    @Before
//    public void setup() throws IOException {
//        String microarrayOrderSource = CharStreams.toString(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("microarrayOrder.txt"), Charsets.UTF_8));
//        String probesetNamesSource = CharStreams.toString(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("probesetNames.txt"), Charsets.UTF_8));
//        String microarraySelectionsSource = CharStreams.toString(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("microarraySelections.txt"), Charsets.UTF_8));
//        String probesetSelectionsSource = CharStreams.toString(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("probesetSelections.txt"), Charsets.UTF_8));
//        microarrayOrder = Splitter.on(',').trimResults().omitEmptyStrings().splitToList(microarrayOrderSource);
//        probesetNames = Splitter.on(',').trimResults().omitEmptyStrings().splitToList(probesetNamesSource);
//        probesetSelections = Splitter.on(',').trimResults().omitEmptyStrings().splitToList(probesetSelectionsSource);
//        microarraySelections = Splitter.on(',').trimResults().omitEmptyStrings().splitToList(microarraySelectionsSource);
//    }

//    @Test
//    public void correlationAnalysis() throws ScriptException, IOException {
//        File dbFile = new File("/home/dcnorris/expressionData");
//        DB db = DBMaker.newFileDB(dbFile).mmapFileEnable().transactionDisable().asyncWriteEnable().cacheHardRefEnable().make();
//        Map<String, Map<String, Double>> expressionData = db.getTreeMap("map");
//        if (expressionData.isEmpty()) {
//            Reader in = new FileReader(expressionFlatFile);
//            for (CSVRecord record : CSVFormat.DEFAULT.parse(in)) {
//                String probesetName = record.get(0);
//                LinkedHashMap<String, Double> probesetExprValues = new LinkedHashMap<>(22_810);
//                for (int i = 1; i < record.size(); i++) {
//                    probesetExprValues.put(microarrayOrder.get(i - 1), Double.parseDouble(record.get(i)));
//                }
//                expressionData.put(probesetName, probesetExprValues);
//                db.commit();
//            }
//            db.compact();
//            db.commit();
//
//        }
//        ScriptEngineManager manager = new ScriptEngineManager();
//        ScriptEngine engine = manager.getEngineByName("Renjin");
//        BiFunction<List<Double>, List<Double>, Double> cor = (x, y) -> {
//            double t = 0;
//            try {
//                engine.put("x", Doubles.toArray(x));
//                engine.put("y", Doubles.toArray(y));
//                engine.eval("result = cor.test(x,y)");
//                t = ((SEXP) engine.eval("result$estimate")).asReal();
//                double pVal = ((SEXP) engine.eval("result$p.value")).asReal();
//            } catch (ScriptException ex) {
//            }
//            return t;
//        };
//        Function<String, List<Double>> probesetExprVals = probeset
//                -> microarraySelections.parallelStream().map(microarray -> expressionData.get(probeset).get(microarray)).collect(toList());
//        List<List<Double>> xVals = probesetNames.parallelStream().map(probeset -> probesetExprVals.apply(probeset)).collect(toList());
//        List<List<Double>> yVals = probesetSelections.parallelStream().map(probeset -> probesetExprVals.apply(probeset)).collect(toList());
//        List<Double> results = xVals.stream().map(x -> yVals.stream().map(y -> cor.apply(x, y))).flatMap(s -> s.parallel()).collect(toList());
//        results.stream().forEach(d -> System.out.println(d));
//    }

}
