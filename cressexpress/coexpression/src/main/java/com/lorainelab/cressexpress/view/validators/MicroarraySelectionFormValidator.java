/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lorainelab.cressexpress.view.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author dcnorris
 */
@FacesValidator("microarraySelectionValidator")
public class MicroarraySelectionFormValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component,
            Object value) throws ValidatorException {

        Integer selectionCount = (Integer) component.getAttributes().get("selectionCount");
        
        if (selectionCount < 1) {
            throw new ValidatorException(new FacesMessage(
                    "You must select at least 1 microarray."));
        }

    }
}
