/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lorainelab.cressexpress.view;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.SetMultimap;
import com.lorainelab.cressexpress.model.MatchMethod;
import com.lorainelab.cressexpress.reference.AgiMappingReference;
import com.lorainelab.cressexpress.reference.MatchMethodReference;
import com.lorainelab.cressexpress.reference.ProbesetInfoReference;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.ejb.ConcurrencyManagement;
import static javax.ejb.ConcurrencyManagementType.BEAN;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dcnorris
 */
@Named
@ConcurrencyManagement(BEAN)
@Stateful
@SessionScoped
public class GeneSelection implements Serializable {

    private static final long serialVersionUID = 7526471155322776147L;

    private static final Logger logger = LoggerFactory.getLogger(GeneSelection.class);

    private static final int MAX_INPUT_SIZE = 30;
    private static SetMultimap<String, String> agiToProbeset;
    
    @Inject
    private AgiMappingReference agiMappingReference;
    
    @Inject
    private ProbesetInfoReference probesetInfoReference;

    @Inject
    private MatchMethodReference matchMethodReference;

    private IdType idType = IdType.AGI;
    private int currentInputSize = 0;
    private String geneInput;
    private Set<String> validProbeSets;
    private List<String> validAGINames;
    private MatchMethod selectedMatchMethod;
    private List<MatchMethod> matchMethodOptions;

    @PostConstruct
    public void init() {
        agiToProbeset = agiMappingReference.getAgiToProbesetMapping();
        validAGINames = agiMappingReference.getValidAGINames();
        matchMethodOptions = matchMethodReference.getMatchMethodOptions();
    }

    public enum IdType {

        AGI("AGI IDs"),
        PROBESET("Probe set IDs");

        private final String label;

        private IdType(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

    public GeneSelection() {
    }

    public IdType[] getIdTypeValues() {
        return IdType.values();
    }

    public IdType getIdType() {
        return idType;
    }

    public void setIdType(IdType idType) {
        this.idType = idType;
    }

    public int getMaxInputSize() {
        return MAX_INPUT_SIZE;
    }

    public String getGeneInput() {
        return geneInput;
    }

    public void setGeneInput(String geneInput) {
        this.geneInput = geneInput;

    }

    public List<String> autoComplete(String query) {
        List<String> results = new ArrayList<>();
        if (idType == IdType.AGI) {
            for (String key : agiToProbeset.keySet()) {
                if (key.startsWith(query.toUpperCase())) {
                    if (!key.equals(query)) { //remove if exact match suggestion is desired
                        results.add(key);
                    }
                }
            }
        } else {
            Map<String, Integer> probeSetIdMap = probesetInfoReference.getProbesetIdMap();
            for (String key : probeSetIdMap.keySet()) {
                if (key.startsWith(query.toUpperCase())) {
                    if (!key.equals(query)) { //remove if exact match suggestion is desired
                        results.add(key);
                    }
                }
            }
        }
        return results;
    }

    public void validateUserInput(FacesContext fc, UIComponent validate, Object value) {
        if (validProbeSets != null) {
            validProbeSets.clear();
        }
        String userInput = value.toString();
        if (Strings.isNullOrEmpty(userInput)) {
            fc.validationFailed();
            FacesMessage msg = new FacesMessage("At least 1 probeset selection is required.");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            fc.addMessage("Validation failed:", msg);
            fc.renderResponse();
            return;
        }
        List<String> splitInput = Splitter.on(CharMatcher.WHITESPACE
                .or(CharMatcher.is(',')))
                .trimResults()
                .omitEmptyStrings()
                .splitToList(userInput);
        Map<String, String> unrecognizedInput = new HashMap<>();
        Set<String> validProbeSetIdBuilder = new HashSet<>();

        if (idType == IdType.AGI) {
            splitInput.stream().forEach(id -> {
                id = id.toUpperCase();
                if (agiToProbeset.keySet().contains(id)) {
                    validProbeSetIdBuilder.addAll(agiToProbeset.get(id));
                } else {
                    if (validAGINames.contains(id)) {
                        unrecognizedInput.put(id, "is a valid AGI id but cannot be mapped to a probeset");
                    } else {
                        unrecognizedInput.put(id, "is not a recognized AGI id");
                    }
                }
            });
        } else {
            splitInput.stream().forEach(id -> {
                id = id.toLowerCase();
                if (!probesetInfoReference.getProbesetIdMap().containsKey(id)) {
                    unrecognizedInput.put(id, "is not recognized");
                } else {
                    validProbeSetIdBuilder.add(id);
                }
            });
        }
        validProbeSets = validProbeSetIdBuilder;
        if (!unrecognizedInput.isEmpty()) {
            fc.validationFailed();
            FacesMessage msg = new FacesMessage("Validation failed, unrecognized IDs submitted:");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            fc.addMessage("Validation failed", msg);
            for (Map.Entry<String, String> entry : unrecognizedInput.entrySet()) {
                String invalidInput = entry.getKey() + ": " + entry.getValue();
                FacesMessage specificError = new FacesMessage(invalidInput);
                specificError.setSeverity(FacesMessage.SEVERITY_ERROR);
                fc.addMessage(invalidInput, specificError);
            }
            fc.renderResponse();
        }
    }

    public List<MatchMethod> getMatchMethodOptions() {
        return matchMethodOptions;
    }

    public MatchMethod getSelectedMatchMethod() {
        if (selectedMatchMethod == null) {
            selectedMatchMethod = getMatchMethodOptions().get(0);
        }
        return selectedMatchMethod;
    }

    public void setSelectedMatchMethod(MatchMethod selectedMatchMethod) {
        this.selectedMatchMethod = selectedMatchMethod;
    }

    public int getCurrentInputSize() {
        return currentInputSize;
    }

    public void setCurrentInputSize(int currentInputSize) {
        this.currentInputSize = currentInputSize;
    }

    public Set<String> getValidProbeSets() {
        return validProbeSets;
    }

    public void navigateNext() throws IOException {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        context.redirect(context.getRequestContextPath() + "/step3.xhtml");
    }

    public void navigatePrev() throws IOException {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        context.redirect(context.getRequestContextPath() + "/step1.xhtml");
    }

}
