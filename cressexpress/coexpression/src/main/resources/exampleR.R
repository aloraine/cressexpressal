# the base URL
url = 'http://www.cressexpress.org/cgi-bin/getExpVals.py'

# probe sets interrogating genes in the glucosinolate biosynthesis pathway
pss = c('252827_at', '264052_at', '253534_at', '263714_at', '264873_at', '260387_at')

# make them into a comma-separated string
pss_string = paste(pss,collapse=',')

# build the URL
url = paste(c(url,'?version=3_0&desc=1&pss=',pss_string),sep='',collapse='')

# for fun, print it
print('The URL is')
print(url)

# access the data
dat = read.delim(url,sep=',',header=TRUE)

# change the column headers to gene names
names = c('cel','exp','ks','tissue','CYP79B2', 'CYP79B3', 'CYP83B1', 'SUR1', 'UGT74B1', 'AT1G74100','desc')
names(dat) = names

# make a plot showing correlated expression
plot(dat[,5:10],main='Glucosinolate biosynthesis pathway genes co-expression')

# compute correlations for all six genes
print(cor(dat[,5:10]))
