

getRdsFiles=function(d='c4',prefix='cr4frma') {
  files = dir(d)
  v = grep(prefix,files)
  sapply(files[v],function(x){file.path(d,x)})
}

loadPssRds=function(file='pss.rds') {
  readRDS(file)
}
	
# pss - list of probesets, in order we want
# rds_file - saved list output from frma processing			    
getExpMatrix=function(pss=NULL,rds_file=NULL){
  lst=readRDS(rds_file)
  N=length(lst)
  m=matrix(rep(NA,length(pss)*N),ncol=N,nrow=length(pss))
  for (i in seq(1:N)) {
    eset=exprs(lst[[i]])
    if (sum(row.names(eset)!=pss)>0) {
      stop("ERROR")
    }
    m[,i]=eset
  }
  row.names(m)=pss
  colnames(m)=sapply(lst,function(x){sampleNames(phenoData(x))})
  m
}

getBigMatrix=function(pss=NULL,files,fname='BigMatrix.rds'){
  m=getExpMatrix(pss=pss,rds_file=files[1])
  N=length(files)
  for (i in seq(2,N)) {
    newm=getExpMatrix(pss=pss,rds_file=files[i])
    m=cbind(m,newm)
  }
  saveRDS(m,file=fname)
  m
}

# the database table expr_col has a field 